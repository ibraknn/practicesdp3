package com.company.PizzaPractice.Pizza;

import com.company.PizzaPractice.IPizza.Pizza;
import com.company.PizzaPractice.PizzaDecorator;

public class MeatPizza extends PizzaDecorator {

    public MeatPizza(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void cook() {
        super.cook();
        System.out.println("Adding meat...");
    }
}
