package com.company.PizzaPractice.Pizza;

import com.company.PizzaPractice.IPizza.Pizza;

public class SimplePizza implements Pizza {

    @Override
    public void cook() {
        System.out.println("Cooking pizza...");
    }
}
