package com.company.PizzaPractice.Pizza;

import com.company.PizzaPractice.IPizza.Pizza;
import com.company.PizzaPractice.PizzaDecorator;

public class ChickenPizza extends PizzaDecorator {

    public ChickenPizza(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void cook() {
        super.cook();
        System.out.println("Adding chicken to your pizza...");
    }
}
