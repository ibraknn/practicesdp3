package com.company.PizzaPractice.Pizza;

import com.company.PizzaPractice.IPizza.Pizza;
import com.company.PizzaPractice.PizzaDecorator;

public class CheesePizza extends PizzaDecorator {
    public CheesePizza(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void cook() {
        super.cook();
        System.out.println("Adding cheese...");
    }
}
