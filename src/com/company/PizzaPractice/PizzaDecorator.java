package com.company.PizzaPractice;

import com.company.PizzaPractice.IPizza.Pizza;

public abstract class PizzaDecorator implements Pizza {
    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza){
        this.pizza = pizza;
    }

    @Override
    public void cook() {
        this.pizza.cook();
    }
}
