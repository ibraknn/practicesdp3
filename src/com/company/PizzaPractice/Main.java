package com.company.PizzaPractice;

import com.company.PizzaPractice.IPizza.Pizza;
import com.company.PizzaPractice.Pizza.CheesePizza;
import com.company.PizzaPractice.Pizza.ChickenPizza;
import com.company.PizzaPractice.Pizza.MeatPizza;
import com.company.PizzaPractice.Pizza.SimplePizza;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Pizza chickenPizza = new ChickenPizza(new SimplePizza());
        chickenPizza.cook();
        Pizza meatPizza = new CheesePizza(new MeatPizza(new SimplePizza()));
        meatPizza.cook();
    }
}
