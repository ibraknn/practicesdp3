package com.company.DiscountPractice;

import com.company.DiscountPractice.IDiscount.IDiscount;

public abstract class DiscountDecorator implements IDiscount {
    protected IDiscount iDiscount;

    public DiscountDecorator(IDiscount iDiscount) {
        this.iDiscount = iDiscount;
    }

    @Override
    public void discount() {
        this.iDiscount.discount();
    }
}
