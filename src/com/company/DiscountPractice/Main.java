package com.company.DiscountPractice;

import com.company.DiscountPractice.Discounts.Discount;
import com.company.DiscountPractice.Discounts.MembershipDiscount;
import com.company.DiscountPractice.Discounts.NewMemberDiscount;
import com.company.DiscountPractice.Discounts.StudentDiscount;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class Main {

    public static void main(String[] args) {
	// write your code here
        IDiscount d1 = new MembershipDiscount(new StudentDiscount(new Discount()));
        d1.discount();
        System.out.println("------------------");
        IDiscount d2 = new NewMemberDiscount(new MembershipDiscount(new Discount()));
        d2.discount();
    }
}
