package com.company.DiscountPractice.Discounts;

import com.company.DiscountPractice.DiscountDecorator;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class MembershipDiscount extends DiscountDecorator {
    public MembershipDiscount(IDiscount iDiscount) {
        super(iDiscount);
    }

    @Override
    public void discount() {
        super.discount();
        System.out.println("membership discount - 10%");
    }
}
