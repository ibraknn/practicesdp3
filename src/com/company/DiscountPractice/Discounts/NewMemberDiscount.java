package com.company.DiscountPractice.Discounts;

import com.company.DiscountPractice.DiscountDecorator;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class NewMemberDiscount extends DiscountDecorator {
    public NewMemberDiscount(IDiscount iDiscount) {
        super(iDiscount);
    }

    @Override
    public void discount() {
        super.discount();
        System.out.println("New member discount - 5%");
    }
}
