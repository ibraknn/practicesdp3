package com.company.DiscountPractice.Discounts;

import com.company.DiscountPractice.DiscountDecorator;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class ElderDiscount extends DiscountDecorator {
    public ElderDiscount(IDiscount iDiscount) {
        super(iDiscount);
    }

    @Override
    public void discount() {
        super.discount();
        System.out.println("elder discount - 15%");
    }
}
