package com.company.DiscountPractice.Discounts;

import com.company.DiscountPractice.DiscountDecorator;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class StudentDiscount extends DiscountDecorator {
    public StudentDiscount(IDiscount iDiscount) {
        super(iDiscount);
    }

    @Override
    public void discount() {
        super.discount();
        System.out.println("student discount - 15%");
    }
}
