package com.company.BirdPractice.IBird;

public interface Bird {
    void fly(String action);
    void makeSound(String action);
}
