package com.company.BirdPractice.IBird;

public interface ToyDuck {
    void squeak(String action, String name);
}
