package com.company.BirdPractice;

import com.company.DiscountPractice.Discounts.Discount;
import com.company.DiscountPractice.Discounts.MembershipDiscount;
import com.company.DiscountPractice.Discounts.NewMemberDiscount;
import com.company.DiscountPractice.Discounts.StudentDiscount;
import com.company.DiscountPractice.IDiscount.IDiscount;

public class Main {

    public static void main(String[] args) {
	// write your code here
        BuilderImplementation builderImplementation = new BuilderImplementation();
        builderImplementation.squeak("fly","bird");
        builderImplementation.squeak("makesound","bird");
        builderImplementation.squeak("makesound","toyduck");
        builderImplementation.squeak("squeak","toyduck");
    }
}
