package com.company.BirdPractice;

import com.company.BirdPractice.IBird.Bird;
import com.company.BirdPractice.IBird.ToyDuck;

public class SqueakBuilder implements ToyDuck {
    Bird bird;

    public SqueakBuilder(String action) {
        if(action.equalsIgnoreCase("Fly")){
            bird = new FlyBuilder();
        }
        else if(action.equalsIgnoreCase("MakeSound")){
            bird = new MakeSoundBuilder();
        }
    }

    @Override
    public void squeak(String action, String name) {
        if(action.equalsIgnoreCase("Fly")){
            bird.fly(action);
        }
        else if(action.equalsIgnoreCase("MakeSound")){
            bird.makeSound(action);
        }
    }
}
