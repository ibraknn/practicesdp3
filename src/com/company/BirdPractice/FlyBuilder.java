package com.company.BirdPractice;

import com.company.BirdPractice.IBird.Bird;
import com.company.BirdPractice.IBird.ToyDuck;

public class FlyBuilder implements Bird {

    @Override
    public void fly(String action) {
        System.out.println("I'm flying and my type is " + action);
    }

    @Override
    public void makeSound(String action) {

    }
}
