package com.company.BirdPractice;

import com.company.BirdPractice.IBird.Bird;

public class MakeSoundBuilder implements Bird {
    @Override
    public void fly(String action) {

    }

    @Override
    public void makeSound(String action) {
        System.out.println("Making sound and my action " + action);
    }
}
