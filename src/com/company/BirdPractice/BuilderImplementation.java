package com.company.BirdPractice;

import com.company.BirdPractice.IBird.ToyDuck;

public class BuilderImplementation implements ToyDuck {
    SqueakBuilder squeakBuilder;
    @Override
    public void squeak(String action, String name) {
        if(action.equalsIgnoreCase("fly")||action.equalsIgnoreCase("makesound")){
            squeakBuilder = new SqueakBuilder(action);
            squeakBuilder.squeak(action,name);
        }
        else{
            System.out.println("Invalid");
        }
    }
}
